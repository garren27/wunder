<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = ['firstName', 'lastName', 'telephone', 'address', 'houseNumber', 'zipCode', 'city', 'accountOwner', 'iban', 'paymentDataId', 'fkState'];
}
