<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\CreateUserBasicRequest;
use App\Http\Requests\CreateUserLocationRequest;
use App\Http\Requests\CreateUserPaymentRequest;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class baseController extends Controller
{
    public function index($error='')
    {
        $user = User::where('fkState', '!=', 3)->first();
        $idUser=0;
        $idState=0;
        if($user!=null) {
            $idUser=$user->id;
            $idState=$user->fkState;
        }
        return view('index', compact('idUser', 'idState', 'error'));
    }

    public function basicSave(CreateUserBasicRequest $request)
    {
        $request['fkState']=1;
        User::create($request->all());
        return redirect()->route('index');
        
    }

    public function locationSave(CreateUserLocationRequest $request)
    {
        $request['fkState']=2;
        User::where('id', $request->input('idusr'))->where('fkState', 1)->firstOrFail()->update($request->all());
        return redirect()->route('index');
    }

    public function paymentSave(CreateUserPaymentRequest $request)
    {
        try{
            $request['fkState']=3;
            $request['paymentDataId']=$this->getPaymentData($request->input('idusr'), $request->input('iban'), $request->input('accountOwner'));
            if($request['paymentDataId']=='')
            {
                return redirect()->route('index', 'It was Impossible to save the Information, try Again');
            }
            else
            {
                User::where('id', $request->input('idusr'))->where('fkState', 2)->firstOrFail()->update($request->all());
                return redirect()->route('success', [$request->input('idusr'), 1]);
            }
        }
        catch (Throwable $t)
        {
            return redirect()->route('index', 'It was Impossible to save the Information, try Again');
        }
        catch(Exception $e)
        {
            return redirect()->route('index', 'It was Impossible to save the Information, try Again');
        }
    }

    public function getPaymentData($customerId, $iban, $owner)
    {
        try{
            $client = new Client(['base_uri' => 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com']);
            $response = $client->request('POST', 'default/wunderfleet-recruiting-backend-dev-save-payment-data', [
                'json' => [
                    'customerId' => $customerId,
                    'iban' => $iban,
                    'owner' => $owner
                ]
            ]);
            $result = json_decode($response->getBody());
            return $result->{'paymentDataId'};
        }
        catch (RequestException $re)
        {
            return "";
        }
        catch (Throwable $t)
        {
            return "";
        }
        catch (Exception $e)
        {
            return "";
        }
    }

    public function success($id, $msg)
    {
        $user = User::where('id', $id)->firstOrFail();
        $message = "";
        if($msg==1)
            $message="Successful registration";
        return view('success', compact('user', 'message'));
    }

    public function userslist()
    {
        $users = User::where('fkState', 3)->get();
        return view('usersList', compact('users'));
    }
}
