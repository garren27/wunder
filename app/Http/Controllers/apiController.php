<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use Carbon\Carbon;
use App\Http\Controllers\baseController;
use Illuminate\Http\Request;
use App\Http\Requests\CreateUserBasicRequest;
use App\Http\Requests\CreateUserLocationRequest;
use App\Http\Requests\CreateUserPaymentRequest;
use GuzzleHttp\Client;

class apiController extends Controller
{
    public function indexApi()
    {
        $user = User::where('fkState', '!=', 3)->first();
        $idUser=0;
        $idState=0;
        if($user!=null) {
            $idUser=$user->id;
            $idState=$user->fkState;
        }
        return view('indexApi', compact('idUser', 'idState'));
    }

    public function basicSaveApi(CreateUserBasicRequest $request)
    {
        try{
            $request['fkState']=1;
            User::create($request->all());
            return json_encode(['status' => 200, "Response" => "Success"], JSON_FORCE_OBJECT);
        }
        catch (Throwable $t)
        {
            return json_encode(['status' => 500, "Response" => $t], JSON_FORCE_OBJECT);
        }
        catch(Exception $e)
        {
            return json_encode(['status' => 500, "Response" => $e], JSON_FORCE_OBJECT);
        }
    }

    public function locationSaveApi(CreateUserLocationRequest $request)
    {
        try{
            $request['fkState']=2;
            User::where('id', $request->input('idusr'))->where('fkState', 1)->firstOrFail()->update($request->all());
            return json_encode(['status' => 200, "Response" => "Success"], JSON_FORCE_OBJECT);
        }
        catch (Throwable $t)
        {
            return json_encode(['status' => 500, "Response" => $t], JSON_FORCE_OBJECT);
        }
        catch(Exception $e)
        {
            return json_encode(['status' => 500, "Response" => $e], JSON_FORCE_OBJECT);
        }
    }

    public function paymentSaveApi(CreateUserPaymentRequest $request)
    {
        try{
            $request['fkState']=3;
            $request['paymentDataId']=app('App\Http\Controllers\baseController')->getPaymentData($request->input('idusr'), $request->input('iban'), $request->input('accountOwner'));
            if($request['paymentDataId']=='')
            {   
                return json_encode(['status' => 500, "Response" => 'It was Impossible to save the Information, try Again'], JSON_FORCE_OBJECT);
            }
            else {
                User::where('id', $request->input('idusr'))->where('fkState', 2)->firstOrFail()->update($request->all());
                return json_encode(['status' => 200, "Response" => "Success", "route" => route('success', [$request->input('idusr'), 1])], JSON_FORCE_OBJECT);    
            }
            
        }
        catch (Throwable $t)
        {
            return json_encode(['status' => 500, "Response" => $t], JSON_FORCE_OBJECT);
        }
        catch(Exception $e)
        {
            return json_encode(['status' => 500, "Response" => $e], JSON_FORCE_OBJECT);
        }
    }
}
