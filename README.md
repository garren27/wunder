
# WUNDER
Small application for the users registration with basic data. 

## How to use 
There is a menu at the upper left where you can see 3 options:

- Sign Up: This option allows the users to do a registration, using only php and making POST and GET request.

- Ajax: This option allows the users to do a registration, using ajax and making apis REST request.

- Users: List of users that have been created so far by any of the two forms mentioned above.

## Starting
When you have finished downloading the project, you will have the source code with the exception of the .env configuration file, the vendor and node_modules folders. In the same way you can see a folder called DB in which you will have the backup for production and test database.


### Pre requirements 📋

It is necessary that the computer or server in which the application will be installed has the following components:

#### Http server and php >= 7
It could be an apache server which can be downloaded and installed from the linux or IOS repositories, or in the case of windows the installation can be done through xampp, laragon, wampp or any other program.

#### Database engine MySQL
It is necessary to have installed the MySQL engine with the permissions to create two databases and perform a restore on them from the backUps that you will find attached in the code (/DB/).

#### Composer
Linux
```
$ curl -sS https://getcomposer.org/installer | php
$ mv composer.phar /usr/local/bin/composer

```

Windows
```
Download installer from https://getcomposer.org/download/ run the downloaded file and follow the steps.

```

#### NPM
Linux
```
apt-get install nodejs

```

Windows

```
Download installer from https://nodejs.org/es/download/ run the downloaded file and follow the steps.

```

#### Databases

Create two databases in MySQL and do a restore from the files that you can find in the path /DB/, the backup called wunder.sql is for production and the wunder_test.sql will be used to run the tests.


### Instalation 🔧

Place the downloaded folder from the repository on the public path of the computer or server.

Install the necessary components for composer.

```
composer install

```

Install the necessary components for npm.

```
npm install

```

You have to create a file called .env in the project root based on the .env.example file,
in this new file you must configure the database section with the information that corresponds to the database installation:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=wunder
DB_DATABASE_TEST=wunder_test
DB_USERNAME=
DB_PASSWORD=
```

Remember that production database name must go in the field DB_DATABASE and the test database name must go in the field DB_DATABASE_TEST.

The path to access the application will be in the folder http://SERVER/PROJECT/public/

### Tests

_In order to execute the integration tests that have been designed in the project, it can be done by console, locating at the project root and executing the command:_

```
phpunit
```

In case of any error happen in windows, or the command is not recognized, the bat file can be executed directly.

```
.\vendor\bin\phpunit.bat
```


## Example 📦
_A functional example of the application can be found on the route:_

http://secure-falls-44431.herokuapp.com



## Built with 🛠️
* [Laravel](https://laravel.com/) - Web Framework
* [JQuery] (https://jquery.com/) - JavaScript Framework
* [Bootstrap] (https://getbootstrap.com/) - Web Framework
* [MySQL] (https://www.mysql.com/) - Database engine
* [VisualStudioCode] (https://code.visualstudio.com/) - IDE


## Author ✒️
* **Carlos Hernán Díaz Brochero** - *Developing* - [garren27](https://bitbucket.org/garren27/)

## Questions

### Describe possible performance optimizations for your Code

_In the index field could keep the current process in memory to avoid constant searches in the database_

_Develop the templates with javascript when the forms are displayed, to avoid access to the server_


### Which things could be done better, than you’ve done it?

_When a user is signing up ask for an identification number. In that way more than one user could do the regitration at the same time just by writting this number and then redirect to the pending view_

_The users list would be showed just to signed in user_

_To be able to edit the information of the previous views either by reloading or in the process directly_

_Generate a better graphic design than the one implemented, generating a better theme or using other tools or frameworks._
