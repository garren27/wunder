<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstName');
            $table->string('lastName');
            $table->string('telephone');
            $table->string('address')->nullable();
            $table->integer('houseNumber')->nullable();
            $table->string('zipCode')->nullable();
            $table->string('city')->nullable();
            $table->string('accountOwner')->nullable();
            $table->string('iban')->nullable();
            $table->string('paymentDataId')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
