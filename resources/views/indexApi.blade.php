@extends('layout')

@section('content')
<div class="login-form">
    <div class="main-div">
        <div class="panel">
            <h2>SIGN UP</h2>
        </div>
        <label id="GeneralError" class="text-danger"></label>
        @if($idUser==0)
        <form method="POST" action="#" id="basicSave">
            <div class="form-group">
                {!! csrf_field() !!}
                <label for="firstName">First Name</label>
                <input type="text" required class="form-control" name="firstName" id="firstName"/>
                <label id="firstNameError" class="text-danger"></label>
            </div>
            <div class="form-group">
                <label for="lastName">Last Name</label>
                <input type="text" required class="form-control" name="lastName" id="lastName"/>
                <label id="lastNameError" class="text-danger"></label>
            </div>
            <div class="form-group">
                <label for="telephone">Telephone Number</label>
                <input type="number" required class="form-control" name="telephone" id="telephone"/>
                <label id="telephoneError" class="text-danger"></label>
            </div>
            <div class="form-group">
                <input type="button" class="btn btn-info" value="Send" onclick="executeBaseSave('basicSave', '{{ route('basicSaveApi') }}')"/>
            </div>
        </form>
        @elseif($idState==1)
        <form method="POST" id="locationSave">
            <div class="form-group">
                {!! csrf_field() !!}
                <input type="hidden" value="{{ $idUser }}" name="idusr" />
                <label for="address">Address</label>
                <input type="text" class="form-control" name="address" id="address"/>
                <label id="addressError" class="text-danger"></label>
            </div>
            <div class="form-group">
                <label for="houseNumber">House Number</label>
                <input type="number" class="form-control" name="houseNumber" id="houseNumber"/>
            </div>
            <div class="form-group">
                <label for="zipCode">Zip Code</label>
                <input type="text" class="form-control" name="zipCode" id="zipCode"/>
            </div>
            <div class="form-group">
                <label for="city">City</label>
                <input type="text" class="form-control" name="city" id="city"/>
                <label id="cityError" class="text-danger"></label>
            </div>
            <div class="form-group">
                <input type="button" class="btn btn-info" value="Send" onclick="executeLocationSave('locationSave', '{{ route('locationSaveApi') }}')"/>
            </div>
        </form>
        @elseif($idState==2)
        <form method="POST" id="paymentSave">
            <div class="form-group">
                {!! csrf_field() !!}
                <input type="hidden" value="{{ $idUser }}" name="idusr" />
                <label for="accountOwner">Account owner</label>
                <input type="text" class="form-control" name="accountOwner" id="accountOwner"/>
                <label id="accountOwnerError" class="text-danger"></label>
            </div>
            <div class="form-group">
                <label for="iban">IBAN</label>
                <input type="text" class="form-control" name="iban" id="iban"/>
                <label id="ibanError" class="text-danger"></label>
            </div>
            <div class="form-group">
                <input type="button" class="btn btn-info" value="Send" onclick="finalSave('paymentSave', '{{ route('paymentSaveApi') }}')"/>
            </div>
        </form>
        @endif
    </div>
</div>


<script src="{{ route('indexa')}}/js/general.js"></script>
@stop
