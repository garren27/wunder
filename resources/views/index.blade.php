@extends('layout')

@section('content')

{{ $error }}
<div class="login-form">
    <div class="main-div">
        <div class="panel">
            <h2>SIGN UP</h2>
        </div>
        @if($idUser==0)
            <form method="POST" action="{{ route('basicSave') }}">
                <div class="form-group">
                    {!! csrf_field() !!}
                    <label for="firstName">First Name</label>
                    <input type="text" class="form-control" name="firstName" id="firstName" value="{{ old('firstName') }}"/>
                    {!! $errors->first('firstName','<span class="text-danger">:message</span>') !!}
                </div>
                <div class="form-group">
                    <label for="lastName">Last Name</label>
                    <input type="text" class="form-control" name="lastName" id="lastName" value="{{ old('lastName') }}"/>
                    {!! $errors->first('lastName','<span class="text-danger">:message</span>') !!}
                </div>
                <div class="form-group">
                    <label for="telephone">Telephone Number</label>
                    <input type="number" class="form-control" name="telephone" id="telephone" value="{{ old('telephone') }}"/>
                    {!! $errors->first('telephone','<span class="text-danger">:message</span>') !!}
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-info" value="Send"/>
                </div>
            </form>
        @elseif($idState==1)
            <form method="POST" action="{{ route('locationSave') }}">
                <div class="form-group">
                    {!! csrf_field() !!}
                    <input type="hidden" value="{{ $idUser }}" name="idusr" />
                    <label for="address">Address</label>
                    <input type="text" class="form-control" name="address" id="address" value="{{ old('address') }}"/>
                    {!! $errors->first('address','<span class="text-danger">:message</span>') !!}
                </div>
                <div class="form-group">
                    <label for="houseNumber">House Number</label>
                    <input type="number" class="form-control" name="houseNumber" id="houseNumber" value="{{ old('houseNumber') }}"/>
                    {!! $errors->first('houseNumber','<span class="text-danger">:message</span>') !!}
                </div>
                <div class="form-group">
                    <label for="zipCode">Zip Code</label>
                    <input type="text" class="form-control" name="zipCode" id="zipCode" value="{{ old('zipCode') }}"/>
                    {!! $errors->first('zipCode','<span class="text-danger">:message</span>') !!}
                </div>
                <div class="form-group">
                    <label for="city">City</label>
                    <input type="text" class="form-control" name="city" id="city" value="{{ old('city') }}"/>
                    {!! $errors->first('city','<span class="text-danger">:message</span>') !!}
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-info" value="Send"/>
                </div>
            </form>
        @elseif($idState==2)
            <form method="POST" action="paymentSave">
                <div class="form-group">
                    {!! csrf_field() !!}
                    <input type="hidden" value="{{ $idUser }}" name="idusr" />
                    <label for="accountOwner">Account owner</label>
                    <input type="text" class="form-control" name="accountOwner" id="accountOwner" value="{{ old('accountOwner') }}"/>
                    {!! $errors->first('accountOwner','<span class="text-danger">:message</span>') !!}
                </div>
                <div class="form-group">
                    <label for="iban">IBAN</label>
                    <input type="text" class="form-control" name="iban" id="iban" value="{{ old('iban') }}"/>
                    {!! $errors->first('iban','<span class="text-danger">:message</span>') !!}
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-info" value="Send"/>
                </div>
            </form>
        @endif
    </div>
</div>



@stop