@extends('layout')

@section('content')
<div class="login-form">
    <div class="main-div-success">
        <div class="panel">
            <h2 class="text-success">SUCCESSFUL REGISTRATION</h2>
        </div><br><br>
        
<label>PAYMENT ID:</label>
<div class="div-success-code">{{ $user->paymentDataId }}</div>
</div>
</div>
@stop