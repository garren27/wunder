@extends('layout')

@section('content')

<div class="login-form">
    <div class="main-div-listusers">
        <div class="panel">
            <h2 class="text-success">USERS</h2>
        </div><br><br>
        <table class="table">
            <thead>
                <th>First Name</th>
                <th>Last Name</th> 
                <th>Address</th> 
                <th>City</th> 
                <th>Payment ID</th> 
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr class="div-success-code">
                    <td>{{ $user->firstName }} </td>
                    <td>{{ $user->lastName }} </td>
                    <td>{{ $user->address }} </td>
                    <td>{{ $user->city }} </td>
                    <td >{{ $user->paymentDataId }} </td>
                </tr>
            @endforeach 
            </tbody>
        </table>    


    </div>
</div>


@stop