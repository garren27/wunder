<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'indexa', 'uses' => 'baseController@index']);

Route::get('/index/{error?}', ['as' => 'index', 'uses' => 'baseController@index']);

Route::post('basicSave', ['as' => 'basicSave', 'uses' => 'baseController@basicSave']);

Route::post('locationSave', ['as' => 'locationSave', 'uses' => 'baseController@locationSave']);

Route::post('paymentSave', ['as' => 'paymentSave', 'uses' => 'baseController@paymentSave']);

Route::get('/userslist', ['as' => 'userslist', 'uses' => 'baseController@userslist']);

Route::get('success/{id}/{msg}', ['as' => 'success', 'uses' => 'baseController@success']);

Route::get('/ApiIndex', ['as' => 'ApiIndex', 'uses' => 'apiController@indexApi']);

Route::post('basicSaveApi', ['as' => 'basicSaveApi', 'uses' => 'apiController@basicSaveApi']);

Route::post('locationSaveApi', ['as' => 'locationSaveApi', 'uses' => 'apiController@locationSaveApi']);

Route::post('paymentSaveApi', ['as' => 'paymentSaveApi', 'uses' => 'apiController@paymentSaveApi']);
