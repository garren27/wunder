<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\State;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use App\Http\Requests\CreateUserBasicRequest;
use App\Http\Requests\CreateUserLocationRequest;
use App\Http\Requests\CreateUserPaymentRequest;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Http\Controllers\baseController;

class baseControllerTest extends TestCase
{
    use DatabaseMigrations;
    
    public function setUp()
    {
        $this->repo = new baseController;
        parent::setUp();

        State::create(['id' => 1, 'state' => 'State1']);
        State::create(['id' => 2, 'state' => 'State2']);
        State::create(['id' => 3, 'state' => 'State3']);
    }

    public function tearDown()
    {
        User::destroy(User::all());
    }

    /** @test */
    public function validate_basicSave_Base()
    {
        
        $request = new CreateUserBasicRequest([
            'firstName' => 'Name',
            'lastName' => 'LastName',
            'telephone' => '1234567890',
        ]);

        $this->assertDatabaseMissing('users', [
            'firstName' => 'Name',
            'lastName' => 'LastName',
            'telephone' => '1234567890',
            'fkState' => 1,
        ]);

        $this->repo->basicSave($request);

        $this->assertDatabaseHas('users', [
            'firstName' => 'Name',
            'lastName' => 'LastName',
            'telephone' => '1234567890',
            'fkState' => 1,
        ]);

        $users = User::where('fkState', 1)->get();
        $this->assertEquals($users->count(), 1);
    }

    /** @test */
    public function validate_locationSave_Base()
    {
        $user = factory('App\User', 1)->create(['fkstate' => 1]);
        $request = new CreateUserLocationRequest([
            'idusr' => $user->first()->id,
            'address' => 'Street 123 ',
            'houseNumber' => '32',
            'zipCode' => '123890',
            'city' => 'Bogota',
        ]);

        $this->assertDatabaseMissing('users', [
            'id' => $user->first()->id,
            'fkState' => 2,
        ]);

        $this->repo->locationSave($request);
        
        $this->assertDatabaseHas('users', [
            'id' => $user->first()->id,
            'fkState' => 2,
        ]);

        $users = User::where('fkState', 2)->get();
        $this->assertEquals($users->count(), 1);
    }

    /** @test */
    public function validate_external_Api()
    {
        $result = $this->repo->getPaymentData(1, '43567', 'owner');
        $this->assertTrue($result!="");
    }

    /** @test */
    public function validate_paymentSave_Base()
    {
        $user = factory('App\User', 1)->create(['fkstate' => 2]);
        $request = new CreateUserPaymentRequest([
            'idusr' => $user->first()->id,
            'accountOwner' => 'Owner',
            'iban' => 'DE12345',
        ]);

        $this->assertDatabaseMissing('users', [
            'id' => $user->first()->id,
            'fkState' => 3,
        ]);
        $this->repo->paymentSave($request);
        
        $this->assertDatabaseHas('users', [
            'id' => $user->first()->id,
            'fkState' => 3,
        ]);
    }


}
