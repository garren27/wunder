function executeBaseSave(formName, route)
    {
        var url = route;
		var formData = new FormData($("form#"+formName)[0]);
        valid=true;
        $('#firstNameError').html("");
        $('#lastNameError').html("");
        $('#telephoneError').html("");
		if($('#firstName').val()=="")
        {
            $('#firstNameError').html('Required');
            valid=false
        }	
        if($('#lastName').val()=="")
        {
            $('#lastNameError').html('Required');
            valid=false
        }	
        if($('#telephone').val()=="")
        {
            $('#telephoneError').html('Required');
            valid=false
        }	
        if(valid)
        {
            $.ajax({
                type: "POST",
                url: url,
                data: formData, 
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function(){
                    
                },	
                success: function(response)
                {
                    var data = JSON.parse(response);
                    if(data['status']==200)
                        location.reload();
                    else
                        $('#GeneralError').html(data['Response']);
                },
                error: function(xhr) { 
                    $('#GeneralError').html('It was Impossible to save the Information, try Again');
                }

            });
        }
    }
    function executeLocationSave(formName, route)
    {
        var url = route;
		var formData = new FormData($("form#"+formName)[0]);
        
        valid=true;
        $('#addressError').html("");
        $('#cityError').html("");

		if($('#address').val()=="")
        {
            $('#addressError').html('Required');
            valid=false
        }	
        if($('#city').val()=="")
        {
            $('#cityError').html('Required');
            valid=false
        }	
        if(valid)
        {
            $.ajax({
                type: "POST",
                url: url,
                data: formData, 
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function(){
                    
                },	
                success: function(response)
                {
                    var data = JSON.parse(response);
                    if(data['status']==200)
                        location.reload();
                    else
                        $('#GeneralError').html(data['Response']);
                },
                error: function(xhr) { 
                    $('#GeneralError').html('It was Impossible to save the Information, try Again');
                }

            });
        }
    }
    function finalSave(formName, route)
    {
        var url = route;
		var formData = new FormData($("form#"+formName)[0]);
				
        valid=true;
        $('#accountOwnerError').html("");
        $('#ibanError').html("");

        if($('#accountOwner').val()=="")
        {
            $('#accountOwnerError').html('Required');
            valid=false
        }	
        if($('#iban').val()=="")
        {
            $('#ibanError').html('Required');
            valid=false
        }

        if(valid)
        {
            $.ajax({
                type: "POST",
                url: url,
                data: formData, 
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function(){
                    
                },	
                success: function(response)
                {
                    var data = JSON.parse(response);
                    if(data['status']==200)
                        location.href=data['route'];
                    else
                        $('#GeneralError').html(data['Response']);
                },
                error: function(xhr) { 
                    $('#GeneralError').html('It was Impossible to save the Information, try Again');
                }

            });
        }
    }